/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   x_type.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 20:48:07 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 12:16:27 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		x_check_minus_flag(t_list *list, char symbol)
{
	char *tmp;

	if (list->flags & MINUS_FLAG)
	{
		tmp = list->s1;
		list->s1 = check_minus_size(list->width_flag, list);
		free(tmp);
		if (list->s1 == NULL)
			return (-1);
	}
	else
	{
		tmp = list->s1;
		list->s1 = check_size(0, list, symbol);
		free(tmp);
		if (list->s1 == NULL)
			return (-1);
	}
	return (1);
}

static int		x_acc_width_flag(t_list *list)
{
	char *tmp;

	if (list->ac_flag == 0)
	{
		if (list->u == 0)
		{
			list->flags |= MINUS_FLAG;
			tmp = list->s1;
			list->s1 = ft_strdup("");
			free(tmp);
			if (list->s1 == NULL)
				return (-1);
		}
	}
	tmp = list->s1;
	list->s1 = check_size(1, list, '0');
	free(tmp);
	if (list->s1 == NULL)
		return (-1);
	return (1);
}

static int		x_width_flag(t_list *list)
{
	char	symbol;

	if (list->width_flag < 0)
	{
		list->flags |= MINUS_FLAG;
		list->width_flag = -(list->width_flag);
	}
	if ((list->flags & ZERO_FLAG) != 0)
	{
		symbol = '0';
		if ((list->flags & WIDTH_FLAG || list->flags & NUM_FLAG)
		&& (list->flags & ACCURACY_ORIG_FLAG
		|| list->accuracy_flag & WIDTH_FLAG)
		&& list->ac_flag >= 0)
			symbol = 'n';
	}
	else
		symbol = 'n';
	if (x_check_minus_flag(list, symbol) == -1)
		return (-1);
	return (1);
}

int				x_hex(va_list args, t_list *list)
{
	list->u = va_arg(args, unsigned int);
	if (!(list->s1 = ft_itoa_base(list->u, 16, 'x')))
		return (-1);
	if (((list->flags & ACCURACY_ORIG_FLAG)
	|| (list->accuracy_flag & WIDTH_FLAG)) && list->ac_flag >= 0)
		if (x_acc_width_flag(list) == -1)
			return (-1);
	if (list->flags & WIDTH_FLAG || list->flags & NUM_FLAG)
		if (x_width_flag(list) == -1)
			return (-1);
	ft_putstr(list->s1, list);
	return (1);
}
