/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_funcs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 14:16:25 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 12:36:58 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*check_minus_size(int zero_flag, t_list *list)
{
	int		len;
	int		min;
	int		i;
	char	*fin_str;
	char	*tmp;

	min = 0;
	i = 0;
	len = ft_strlen(list->s1);
	if (zero_flag > len)
		min = zero_flag - len;
	else
		return (ft_strdup(list->s1));
	if (!(fin_str = (char *)malloc(sizeof(char) * (min + 1))))
		return (NULL);
	while (i < min)
	{
		fin_str[i] = ' ';
		i++;
	}
	fin_str[i] = '\0';
	tmp = fin_str;
	fin_str = ft_strjoin(list->s1, fin_str);
	free(tmp);
	return (fin_str);
}

char	*check_str_size(int zero_flag, t_list *list)
{
	int		i;
	char	*ptr;

	i = zero_flag;
	if (!(ptr = (char *)malloc(sizeof(char) * (i + 1))))
		return (NULL);
	i = 0;
	if (zero_flag >= 0)
	{
		while (list->s1[i] != '\0' && i < zero_flag - 1)
		{
			ptr[i] = list->s1[i];
			i++;
		}
		ptr[i] = '\0';
	}
	return (ptr);
}

void	check_minus_char(int zero_flag, t_list *list, char mode)
{
	int		min;
	int		i;
	char	a;

	a = (list->flags & ZERO_FLAG ? '0' : ' ');
	min = zero_flag - 1;
	if (min <= 0)
	{
		ft_putchar(list->c, list);
		return ;
	}
	i = 0;
	if (mode == '-')
	{
		ft_putchar(list->c, list);
		while (i++ < min)
			ft_putchar(' ', list);
	}
	if (mode == 'n')
	{
		while (i++ < min)
			ft_putchar(a, list);
		ft_putchar(list->c, list);
	}
}
