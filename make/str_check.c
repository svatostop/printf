/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_check.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 20:29:43 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 14:41:29 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		str_check_minus_flag(t_list *list, char symbol)
{
	char *tmp;

	if (list->flags & MINUS_FLAG)
	{
		tmp = list->s1;
		list->s1 = check_minus_size(list->width_flag, list);
		free(tmp);
		if (list->s1 == NULL)
			return (-1);
	}
	else
	{
		tmp = list->s1;
		list->s1 = check_size(0, list, symbol);
		free(tmp);
		if (list->s1 == NULL)
			return (-1);
	}
	return (1);
}

static int		str_acc_width_flag(t_list *list)
{
	char	*tmp;
	int		len;

	len = ft_strlen(list->s1);
	if (list->ac_flag >= 0 && (list->ac_flag < len))
	{
		tmp = list->s1;
		list->s1 = check_str_size(list->ac_flag + 1, list);
		free(tmp);
		if (list->s1 == NULL)
			return (-1);
	}
	return (1);
}

static int		str_char_width_flag(t_list *list)
{
	char	symbol;

	if (list->width_flag < 0)
	{
		list->flags |= MINUS_FLAG;
		list->width_flag = -(list->width_flag);
	}
	if ((list->flags & ZERO_FLAG) != 0)
		symbol = '0';
	else
		symbol = 'n';
	if (str_check_minus_flag(list, symbol) == -1)
		return (-1);
	return (1);
}

int				str_char(va_list args, t_list *list)
{
	list->s1 = va_arg(args, char *);
	if (list->s1 == NULL)
		list->s1 = ft_strdup("(null)");
	else if (list->s1[0] == '\0')
		list->s1 = ft_strdup("");
	else
		list->s1 = ft_strdup(list->s1);
	if (!list->s1)
		return (-1);
	if (list->flags & ACCURACY_ORIG_FLAG || list->accuracy_flag & WIDTH_FLAG)
		if (str_acc_width_flag(list) == -1)
			return (-1);
	if (list->flags & WIDTH_FLAG || list->flags & NUM_FLAG)
		if (str_char_width_flag(list) == -1)
			return (-1);
	ft_putstr(list->s1, list);
	return (1);
}
