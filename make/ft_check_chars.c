/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_chars.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 20:22:48 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 13:12:19 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	c_char(va_list args, t_list *list)
{
	list->c = va_arg(args, int);
	if (list->flags & MINUS_FLAG)
		check_minus_char(list->width_flag, list, '-');
	else
		check_minus_char(list->width_flag, list, 'n');
}

void	proc_char(t_list *list)
{
	list->c = '%';
	if (list->flags & MINUS_FLAG)
		check_minus_char(list->width_flag, list, '-');
	else
		check_minus_char(list->width_flag, list, 'n');
}

int		check_chars(va_list args, t_list *list)
{
	if (list->type == 'c')
		c_char(args, list);
	if (list->type == 's')
		if (str_char(args, list) == -1)
			return (-1);
	if (list->type == '%')
		proc_char(list);
	return (1);
}
