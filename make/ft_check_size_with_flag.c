/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_size_with_flag.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 22:01:05 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 14:28:50 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*fin_str_symbol(char symbol, t_list *list, int min, int mode)
{
	char	*fin_str;
	char	*tmp;
	int		i;

	i = -1;
	if (!(fin_str = (char *)malloc(sizeof(char) * (min + 1))))
		return (NULL);
	if (list->num < 0 && ((symbol == 'n' && mode == 1)
		|| (symbol == '0')))
	{
		fin_str[0] = '-';
		i++;
		list->s1[0] = '0';
	}
	while (++i < min)
		if (symbol == '0')
			fin_str[i] = '0';
		else
			fin_str[i] = ' ';
	fin_str[i] = '\0';
	tmp = fin_str;
	fin_str = ft_strjoin(fin_str, list->s1);
	free(tmp);
	return (fin_str);
}

char		*check_size(int mode, t_list *list, char symbol)
{
	size_t	len;
	int		min;
	char	*fin_str;

	min = 0;
	if (mode == 0)
		len = list->width_flag;
	else
		len = list->ac_flag;
	if (list->num < 0 && len > ft_strlen(list->s1) - mode)
		min = len - ft_strlen(list->s1);
	else if (len > ft_strlen(list->s1))
		min = len - ft_strlen(list->s1);
	else
		return (ft_strdup(list->s1));
	if (list->num < 0 && ((symbol == 'n' && mode == 1)
		|| (symbol == '0' && mode == 1)))
		min++;
	fin_str = fin_str_symbol(symbol, list, min, mode);
	return (fin_str);
}
