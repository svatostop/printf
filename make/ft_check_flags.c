/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_flags.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/14 13:15:41 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 14:34:22 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		check_flag_width(char *str, int *j, t_list *list, int *i)
{
	if (str[*j] == '*')
	{
		list->flags |= WIDTH_FLAG;
	}
	else
	{
		list->flags = list->flags | NUM_FLAG;
		list->width_flag = ft_atoi(str + (*j));
		while (check_digit(str[*j]))
		{
			(*j)++;
			(*i)++;
		}
		(*i)--;
		(*j)--;
	}
	return (1);
}

int		check_flag_acc(char *str, int *j, t_list *list, int *i)
{
	if (str[*j] == '.')
	{
		(*j)++;
		if (str[*j] == '*')
		{
			(*i)++;
			list->accuracy_flag = list->accuracy_flag | WIDTH_FLAG;
		}
		else
		{
			list->flags = list->flags | ACCURACY_ORIG_FLAG;
			list->ac_flag = ft_atoi(str + (*j));
			while (check_digit(str[*j]))
			{
				(*j)++;
				(*i)++;
			}
		}
	}
	return (1);
}

int		check_type_struct(va_list args, t_list *list)
{
	if (list->type == 'd' || list->type == 'u' || list->type == 'i')
	{
		if (check_dec(args, list) == -1)
			return (-1);
	}
	if (list->type == 'c' || list->type == 's' || list->type == '%')
	{
		if (check_chars(args, list) == -1)
			return (-1);
	}
	if (list->type == 'x' || list->type == 'X' || list->type == 'p')
	{
		if (check_hex(args, list) == -1)
			return (-1);
	}
	return (1);
}
