/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 14:14:54 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 14:26:46 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <unistd.h>
# include <stdlib.h>

# define NO_FLAG 			0b00000000
# define ZERO_FLAG 			0b00000001
# define MINUS_FLAG 		0b00000010
# define NUM_FLAG 			0b00000100
# define WIDTH_FLAG 		0b00001000
# define ACCURACY_ORIG_FLAG 0b00010000

typedef struct				s_list
{
	char					type;
	int						symbols;
	char					accuracy_flag;
	char					flags;
	int						num;
	int						width_flag;
	int						ac_flag;
	unsigned long int		p_num;
	char					*s1;
	char					c;
	unsigned int			u;
}							t_list;

char						*ft_itoa_base(unsigned int n, int base, char mode);
int							ft_printf(const char *str, ...);
int							ft_isdigit(char *ch);
int							check_type_struct(va_list args, t_list *list);
void						ft_putnbr(int n, t_list *list, char mode);
void						ft_putstr(char *s, t_list *list);
void						ft_putchar(char c, t_list *list);
char						*ft_itoa(int n);
t_list						*ft_lstnew(void);
char						*p_to_str(unsigned long int n, int base);
size_t						ft_strlcpy(char *dst, const char *src, size_t size);
size_t						ft_strlen(const char *str);
char						*ft_strjoin(char const *s1, char const *s2);
int							ft_atoi(const char *str);
int							ft_isalpha(int ch);
char						*ft_strdup(char *str);
char						*check_minus_size(int zero_flag, t_list *list);
char						*check_str_size(int zero_flag, t_list *list);
void						check_minus_char(int zero_flag, t_list *list,
								char mode);
int							check_flag_zero(char *str, int j, t_list *list,
								int *i);
int							check_flag_minus(char *str, int j, t_list *list,
								int *i);
int							check_flag_acc(char *str, int *j,
								t_list *list, int *i);
int							check_flag_width(char *str, int *j,
								t_list *list, int *i);
int							check_digit(char ch);
int							check_chars(va_list args, t_list *list);
int							check_hex(va_list args, t_list *list);
int							check_dec(va_list args, t_list *list);
int							u_decimal(va_list args, t_list *list);
int							i_decimal(va_list args, t_list *list);
int							d_decimal(va_list args, t_list *list);
void						c_char(va_list args, t_list *list);
int							str_char(va_list args, t_list *list);
void						proc_char(t_list *list);
int							pointer_type(va_list args, t_list *list);
int							xx_hex(va_list args, t_list *list);
int							x_hex(va_list args, t_list *list);
char						*check_size(int mode, t_list *list, char symbol);
int							clear_all_lst(t_list *list);
int							is_flag(char c);

#endif
