/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 14:06:24 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 14:31:34 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		clear_flags(t_list *list)
{
	list->flags = NO_FLAG;
	list->accuracy_flag = NO_FLAG;
	list->ac_flag = 0;
	list->width_flag = 0;
}

static int		check_flag(char *str, t_list *list, int *i)
{
	int j;

	j = 0;
	while (is_flag(str[j]))
	{
		if (str[j] == '0')
			list->flags = list->flags | ZERO_FLAG;
		else if (str[j] == '*' || (check_digit(str[j]) == 1))
			check_flag_width(str, &j, list, i);
		else if (str[j] == '.')
			check_flag_acc(str, &j, list, i);
		else if (str[j] == '-')
			list->flags |= MINUS_FLAG;
		j++;
		(*i)++;
	}
	return (1);
}

static int		check_type(char *str, va_list args, t_list *list)
{
	int i;

	i = 0;
	if (str[i] == 'd')
		list->type = 'd';
	else if (str[i] == 's')
		list->type = 's';
	else if (str[i] == 'c')
		list->type = 'c';
	else if (str[i] == 'x')
		list->type = 'x';
	else if (str[i] == 'X')
		list->type = 'X';
	else if (str[i] == 'i')
		list->type = 'i';
	else if (str[i] == 'u')
		list->type = 'u';
	else if (str[i] == 'p')
		list->type = 'p';
	else if (str[i] == '%')
		list->type = '%';
	if (check_type_struct(args, list) == -1)
		return (-1);
	return (1);
}

static int		check_flags_and_types(char *ptr, int *i,
										t_list *list, va_list args)
{
	check_flag(ptr + ++(*i), list, i);
	if (list->flags & WIDTH_FLAG)
		list->width_flag = va_arg(args, int);
	if (list->accuracy_flag & WIDTH_FLAG)
		list->ac_flag = va_arg(args, int);
	if (check_type(ptr + (*i), args, list) == -1)
		return (-1);
	clear_flags(list);
	return (1);
}

int				ft_printf(const char *str, ...)
{
	int		len;
	va_list	args;
	char	*ptr;
	t_list	*list;

	if (!(list = ft_lstnew()))
		return (-1);
	ptr = (char*)str;
	va_start(args, str);
	len = 0;
	while (ptr[len] != '\0')
	{
		while (ptr[len] != '\0' && ptr[len] != '%')
			ft_putchar(ptr[len++], list);
		if (ptr[len] == '%' && ptr[len + 1])
			if (check_flags_and_types(ptr, &len, list, args) == -1
			&& clear_all_lst(list))
				return (-1);
		if (ptr[len])
			len++;
	}
	len = list->symbols;
	free(list);
	return (len);
}
