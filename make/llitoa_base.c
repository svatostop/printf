/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llitoa_base.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 19:55:30 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/18 13:11:58 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char		*num_fin(unsigned long int n, char *str, int i)
{
	if (n == 10)
		str[i] = 'a';
	if (n == 11)
		str[i] = 'b';
	if (n == 12)
		str[i] = 'c';
	if (n == 13)
		str[i] = 'd';
	if (n == 14)
		str[i] = 'e';
	if (n == 15)
		str[i] = 'f';
	return (str);
}

char			*ft_llitoa_base(unsigned long int n, int base,
				unsigned long int count, unsigned long int num)
{
	unsigned long int	i;
	char				*str;

	i = 0;
	while (count / base)
	{
		count = count / base;
		i++;
	}
	i++;
	if (!(str = (char*)malloc(sizeof(char) * (i + 1))))
		return (NULL);
	str[i] = '\0';
	while (i--)
	{
		num = n % base;
		if (num <= 9)
			str[i] = num + '0';
		if (num >= 10 && num <= 15)
			num_fin(num, str, i);
		n = n / base;
	}
	return (str);
}

char			*p_to_str(unsigned long int n, int base)
{
	unsigned long int	num;
	unsigned long int	count;
	char				*tmp;

	count = n;
	num = 0;
	tmp = ft_llitoa_base(n, base, count, num);
	return (tmp);
}
