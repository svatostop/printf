/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_hex.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 20:40:48 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/19 19:59:16 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		p_check_minus_flag(t_list *list, char symbol)
{
	char *tmp;

	if (list->flags & MINUS_FLAG)
	{
		tmp = list->s1;
		list->s1 = check_minus_size(list->width_flag, list);
		free(tmp);
		if (list->s1 == NULL)
			return (-1);
	}
	else
	{
		tmp = list->s1;
		list->s1 = check_size(0, list, symbol);
		free(tmp);
		if (list->s1 == NULL)
			return (-1);
	}
	return (1);
}

static int		p_acc_width_flag(t_list *list)
{
	char *tmp;

	tmp = list->s1;
	list->s1 = check_size(1, list, '0');
	free(tmp);
	if (list->s1 == NULL)
		return (-1);
	return (1);
}

static int		p_width_flag(t_list *list)
{
	char	symbol;

	if (list->width_flag < 0)
	{
		list->flags |= MINUS_FLAG;
		list->width_flag = -(list->width_flag);
	}
	if ((list->flags & ZERO_FLAG) != 0)
	{
		symbol = '0';
		if ((list->flags & WIDTH_FLAG || list->flags & NUM_FLAG)
		&& (list->flags & ACCURACY_ORIG_FLAG
		|| list->accuracy_flag & WIDTH_FLAG) && list->ac_flag >= 0)
			symbol = 'n';
	}
	else
		symbol = 'n';
	if (p_check_minus_flag(list, symbol) == -1)
		return (-1);
	return (1);
}

int				pointer_type(va_list args, t_list *list)
{
	char *tmp;

	list->p_num = va_arg(args, unsigned long int);
	if (list->p_num == 0 && ((list->flags & ACCURACY_ORIG_FLAG)
	|| (list->accuracy_flag & WIDTH_FLAG)) && list->ac_flag == 0)
		list->s1 = ft_strdup("");
	else
		list->s1 = p_to_str(list->p_num, 16);
	if (!list->s1)
		return (-1);
	if (((list->flags & ACCURACY_ORIG_FLAG)
	|| (list->accuracy_flag & WIDTH_FLAG))
	&& list->ac_flag > ((int)ft_strlen(list->s1)))
		if (p_acc_width_flag(list) == -1)
			return (-1);
	tmp = list->s1;
	list->s1 = ft_strjoin("0x", list->s1);
	free(tmp);
	if (!list->s1)
		return (-1);
	if (list->flags & WIDTH_FLAG || list->flags & NUM_FLAG)
		if (p_width_flag(list) == -1)
			return (-1);
	ft_putstr(list->s1, list);
	return (1);
}

int				check_hex(va_list args, t_list *list)
{
	if (list->type == 'x')
	{
		if (x_hex(args, list) == -1)
			return (-1);
	}
	if (list->type == 'X')
	{
		if (xx_hex(args, list) == -1)
			return (-1);
	}
	if (list->type == 'p')
	{
		if (pointer_type(args, list) == -1)
			return (-1);
	}
	return (1);
}
