/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_funcs_sec.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 21:56:53 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/18 14:00:04 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putchar(char c, t_list *list)
{
	write(1, &c, 1);
	list->symbols++;
}

void	ft_putstr(char *s, t_list *list)
{
	int i;

	if (!s)
		return ;
	i = 0;
	while (s[i])
	{
		ft_putchar(s[i], list);
		i++;
	}
	free(s);
}

void	ft_putnbr(int n, t_list *list, char mode)
{
	unsigned int a;

	if (mode == 'd')
	{
		if (n == -2147483648)
		{
			ft_putstr("-2147483648", list);
			return ;
		}
		if (n < 0)
		{
			ft_putchar('-', list);
			n = -n;
		}
		if (n / 10)
			ft_putnbr(n / 10, list, 'd');
		ft_putchar(n % 10 + '0', list);
	}
	if (mode == 'u')
	{
		a = (unsigned int)n;
		if (a / 10)
			ft_putnbr(a / 10, list, 'u');
		ft_putchar(a % 10 + '0', list);
	}
}

int		check_digit(char ch)
{
	if (ch >= '0' && ch <= '9')
		return (1);
	return (0);
}

int		is_flag(char c)
{
	if (check_digit(c) || c == '0' || c == '.' || c == '*' || c == '-')
		return (1);
	return (0);
}
