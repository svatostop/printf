/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_funcs_third.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/15 21:59:09 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/21 14:27:00 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_list	*ft_lstnew(void)
{
	t_list *tmp;

	if (!(tmp = (t_list*)malloc(sizeof(t_list))))
		return (NULL);
	if (tmp != NULL)
	{
		tmp->flags = NO_FLAG;
		tmp->accuracy_flag = NO_FLAG;
		tmp->width_flag = 0;
		tmp->symbols = 0;
		tmp->ac_flag = 0;
		tmp->type = '\0';
		tmp->num = 0;
		tmp->s1 = NULL;
		tmp->c = '\0';
		tmp->u = 0;
		tmp->p_num = 0;
	}
	return (tmp);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	unsigned int	s1_len;
	unsigned int	s2_len;
	char			*str;

	if (!s1 && !s2)
		return (NULL);
	s1_len = ft_strlen(s1);
	s2_len = ft_strlen(s2);
	if (!(str = (char*)malloc(sizeof(char) * (s1_len + s2_len + 1))))
		return (NULL);
	ft_strlcpy(str, s1, s1_len + 1);
	ft_strlcpy(str + s1_len, s2, s2_len + 1);
	return (str);
}

size_t	ft_strlen(const char *str)
{
	size_t i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

size_t	ft_strlcpy(char *dst, const char *src, size_t size)
{
	size_t	i;
	int		j;

	j = 0;
	i = 0;
	if (!dst || !src)
		return (0);
	while (src[j] != '\0')
		j++;
	if (size > 0)
	{
		while (src[i] != '\0' && i < size - 1)
		{
			dst[i] = src[i];
			i++;
		}
		dst[i] = '\0';
	}
	return (j);
}

int		clear_all_lst(t_list *list)
{
	if (list->s1 != NULL)
		free(list->s1);
	list->s1 = NULL;
	free(list);
	return (1);
}
