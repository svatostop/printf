/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/04 13:19:13 by lgorilla          #+#    #+#             */
/*   Updated: 2020/07/19 19:37:58 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char		*num_fin_xx(unsigned int n, char *str, int i, char mode)
{
	if (mode == 'X')
	{
		if (n == 10)
			str[i] = 'A';
		if (n == 11)
			str[i] = 'B';
		if (n == 12)
			str[i] = 'C';
		if (n == 13)
			str[i] = 'D';
		if (n == 14)
			str[i] = 'E';
		if (n == 15)
			str[i] = 'F';
	}
	return (str);
}

static char		*num_fin_x(unsigned int n, char *str, int i, char mode)
{
	if (mode == 'x')
	{
		if (n == 10)
			str[i] = 'a';
		if (n == 11)
			str[i] = 'b';
		if (n == 12)
			str[i] = 'c';
		if (n == 13)
			str[i] = 'd';
		if (n == 14)
			str[i] = 'e';
		if (n == 15)
			str[i] = 'f';
	}
	return (str);
}

static char		*check_mode(unsigned int i, unsigned int n, int base, char mode)
{
	unsigned int	num;
	char			*str;

	num = 0;
	if (!(str = (char*)malloc(sizeof(char) * (i + 1))))
		return (NULL);
	str[i] = '\0';
	while (i--)
	{
		num = n % base;
		if (num <= 9)
			str[i] = num + '0';
		if (mode == 'x')
		{
			if (num >= 10 && num <= 15)
				num_fin_x(num, str, i, mode);
		}
		if (mode == 'X')
		{
			if (num >= 10 && num <= 15)
				num_fin_xx(num, str, i, mode);
		}
		n = n / base;
	}
	return (str);
}

char			*ft_itoa_base(unsigned int n, int base, char mode)
{
	unsigned int	i;
	char			*str;
	unsigned int	count;

	count = n;
	i = 0;
	while (count / base)
	{
		count = count / base;
		i++;
	}
	i++;
	str = check_mode(i, n, base, mode);
	return (str);
}
